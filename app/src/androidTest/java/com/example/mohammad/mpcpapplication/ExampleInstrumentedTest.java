package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.example.mohammad.mpcpapplication.protocol.EncryptionProtocol;
import com.example.mohammad.mpcpapplication.protocol.ProtocolFactory;
import com.example.mohammad.mpcpapplication.utils.Config;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    private static final int SELECTED_NUMBER = 9;
    private Context context;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.example.mohammad.mpcpapplication", appContext.getPackageName());
    }

    @Test
    public void paceMiddleNumberMiddleKey_IsCorrect() throws Exception {
        EncryptionProtocol protocol = ProtocolFactory.createProtocol(context, Config.PACE_PROTOCOL);
        assertEquals(protocol.calculateMiddleKey(SELECTED_NUMBER), 4);
    }

    @Test
    public void diffieHellmanMiddleNumberMiddleKey_IsCorrect() throws Exception {
        EncryptionProtocol protocol = ProtocolFactory.createProtocol(context, Config.DIFFIE_HELLMAN);
        assertEquals(protocol.calculateMiddleKey(SELECTED_NUMBER), 4);
    }
}
