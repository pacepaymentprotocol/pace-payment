package com.example.mohammad.mpcpapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Preferences;

public class ChooseMethodActivity extends AppCompatActivity {

    // Payer choose his preferred method in this activity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_method);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Preferences.init(this);

        final Button diffieButton = (Button) findViewById(R.id.dhBtn);
        diffieButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMethod(diffieButton);
            }
        });

        final Button paceButton = (Button) findViewById(R.id.ppBtn);
        paceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseMethod(paceButton);
            }
        });

    }


    /**
     * Choose a method then go to the next activity
     * @param button
     */
    private void chooseMethod(Button button) {
        switch (button.getId()){
            case R.id.dhBtn:
                Preferences.putString(Config.DEFAULT_METHOD, Config.DIFFIE_HELLMAN);
                break;
            case R.id.ppBtn:
                Preferences.putString(Config.DEFAULT_METHOD, Config.PACE_PROTOCOL);
                break;
            default:
                Preferences.putString(Config.DEFAULT_METHOD, Config.PACE_PROTOCOL);
                break;
        }
        startActivity(new Intent(ChooseMethodActivity.this, SecureChannelPayerActivity.class));
    }

}
