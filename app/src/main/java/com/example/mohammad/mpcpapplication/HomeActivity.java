package com.example.mohammad.mpcpapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;
import com.example.mohammad.mpcpapplication.utils.Preferences;

import java.util.HashMap;

public class HomeActivity extends AppCompatActivity {

    private static final String LOG_TAG = HomeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Preferences.init(this);
        Log.d(LOG_TAG, Preferences.getString(Config.TRANSACTIONS));

        String token = Preferences.getString(Config.REG_TOKEN);
        if(!token.isEmpty()){
            sendRegistrationToServer(token);
        }

        //String method = Preferences.getString(Config.DEFAULT_METHOD);
        Button mSendMoneyButton = (Button) findViewById(R.id.sendMoneyButton);
        mSendMoneyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startSecureChannel();
            }
        });

    }


    private void startSecureChannel() {
        startActivity(new Intent(HomeActivity.this, ChooseMethodActivity.class));
    }



    private void sendRegistrationToServer(String token) {
        Preferences.init(this);
        int userId = Preferences.getInteger(Config.USER_ID);
        new SendTokenToServerTask(String.valueOf(userId), token).execute();
    }


    private class SendTokenToServerTask extends AsyncTask<Object, Object, String> {

        private String userId;
        private final String token;

        SendTokenToServerTask(String userId, String token){
            this.userId = userId;
            this.token = token;
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put("userId", this.userId);
            params.put("push_device_token", this.token);
            return FunctionHelper.apiCaller("user/register-token", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "Registered the token =  " + result );
        }
    }


}
