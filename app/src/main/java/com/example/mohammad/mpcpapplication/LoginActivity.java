package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mohammad.mpcpapplication.models.User;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Encryption;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;
import com.example.mohammad.mpcpapplication.utils.Preferences;
import com.google.gson.Gson;

import java.security.SecureRandom;
import java.util.HashMap;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;


public class LoginActivity extends AppCompatActivity {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    // Controls definitions
    EditText usernameText;
    EditText passwordText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // TODO Vahid remove this
        // Encryption test
        try{
            byte[] keyStart = "this is a key".getBytes();
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
            sr.setSeed(keyStart);
            kgen.init(128, sr); // 192 and 256 bits may not be available
            SecretKey skey = kgen.generateKey();
            byte[] key = skey.getEncoded();
            // Encryption test sample
            byte[] b = "Let's ecnrypt this".getBytes();
            // encrypt
            byte[] encryptedData = Encryption.encrypt(key,b);
            // decrypt
            byte[] decryptedData = Encryption.decrypt(key,encryptedData);
            Log.d(LOG_TAG, "Decrypted data: "+decryptedData);
        }catch(Exception e){
            Log.d(LOG_TAG, "Encryption failure!");
        }

        //Initialize preferences
        Preferences.init(this);
        int timestamp1 = 2;
        Preferences.putInteger(Config.TIMESTAMP1,timestamp1);

        // TODO Taran remove it, This is an example for Taran
        int sharedKey = 2;
        Log.d(LOG_TAG, "shared key in payer side: "+sharedKey);

        // Controls
        usernameText = (EditText)findViewById(R.id.usernameText);
        if(!Preferences.getString(Config.USER_EMAIL).isEmpty()){
            usernameText.setText(Preferences.getString(Config.USER_EMAIL));
        }

        passwordText = (EditText)findViewById(R.id.passwordText);
        if(!Preferences.getString(Config.USER_PASSWORD).isEmpty()){
            passwordText.setText(Preferences.getString(Config.USER_PASSWORD));
        }


        // Login logic
        Button loginButton = (Button) findViewById(R.id.loginBtn);
        loginButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                String email = usernameText.getText().toString();
                String password = passwordText.getText().toString();
                authenticate(email, password);
            }
        });

        // Redirect to register page
        Button registerButton = (Button) findViewById(R.id.registerBtn);
        registerButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                Intent registerPage = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(registerPage);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Set keyboard dismissal
        setTouchListenerForKeyboardDismissal();
    }

    public void setTouchListenerForKeyboardDismissal() {

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_login);
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motion) {

                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void authenticate(String username, String password){

        new LoginTask(username, password).execute();
    }


    private class LoginTask extends AsyncTask<Object, Object, String> {

        private final String username;
        private final String password;

        LoginTask(String username, String password){
            this.username = username;
            this.password = password;
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put("email", this.username);
            params.put("password", this.password);
            return FunctionHelper.apiCaller("auth/login", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "Executed ... " + result);
            if(result != null){
                User user = new Gson().fromJson(result, User.class);
                Preferences.putInteger(Config.USER_ID, user.getId());
                Preferences.putString(Config.USER_EMAIL, user.getEmail());
                Preferences.putString(Config.USER_PASSWORD, this.password);
                Preferences.putBoolean(Config.LOGGED_IN, true);
                Intent secureChannelPayerPage = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(secureChannelPayerPage);
            }else{
                Toast.makeText(LoginActivity.this, "Username or password is wrong", Toast.LENGTH_LONG).show();
            }
        }
    }

}
