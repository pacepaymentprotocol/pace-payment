package com.example.mohammad.mpcpapplication;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.mohammad.mpcpapplication.models.Transaction;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Preferences;

public class MainActivity extends AppCompatActivity {

    /**
     * Duration of wait
     **/
    public static final int SPLASH_DISPLAY_LENGTH = 2000;
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        checkForNotifications(getIntent());

        Preferences.init(this);

        boolean isLoggedIn = Preferences.getBoolean(Config.LOGGED_IN, false);
        Intent mainIntent = new Intent(
                MainActivity.this,
                isLoggedIn  ? HomeActivity.class : LoginActivity.class
        );
        startActivity(mainIntent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // getIntent() should always return the most recent
        setIntent(intent);
        checkForNotifications(intent);
        Log.d(LOG_TAG, "onNewIntent");
    }


    private void checkForNotifications(Intent intent) {
        String action = intent.getAction();
        if(action!=null && action.equals(MyFirebaseMessagingService.SECURE_CHANNEL)) {
            String transactionId = intent.getStringExtra(Config.TRANSACTION_ID);
            Log.d(LOG_TAG, "transaction id = "  + transactionId);
        }
    }
}
