package com.example.mohammad.mpcpapplication;


import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.example.mohammad.mpcpapplication.models.User;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;
import com.example.mohammad.mpcpapplication.utils.Preferences;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import java.util.HashMap;


public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private String LOG_TAG = this.getClass().getSimpleName();

    @Override
    public void onTokenRefresh(){
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(LOG_TAG, "refreshedToken = " + refreshedToken);
        Preferences.init(this);
        Preferences.putString(Config.REG_TOKEN, refreshedToken);
        Log.d(LOG_TAG, refreshedToken);
    }

}