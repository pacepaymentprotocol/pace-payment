package com.example.mohammad.mpcpapplication;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.example.mohammad.mpcpapplication.models.Transaction;
import com.example.mohammad.mpcpapplication.protocol.EncryptionProtocol;
import com.example.mohammad.mpcpapplication.protocol.ProtocolFactory;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String REG_TOKEN = "REG_TOKEN";
    private static final String LOG_TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static final String SECURE_CHANNEL = "secureChannel";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){

        int transactionId = Integer.parseInt(remoteMessage.getData().get(Config.TRANSACTION_ID));
        int step = Integer.parseInt(remoteMessage.getData().get(Config.STEP));
        Log.d(LOG_TAG, "transaction = "  + remoteMessage.getData().toString());
        Log.d(LOG_TAG, "transaction id = "  + transactionId);
        Log.d(LOG_TAG, "step = "  + step);

        Intent intent =  getIntentForStep(step);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(Config.TRANSACTION_ID, transactionId);
        if(step ==  Transaction.STEP_TWO){
            Transaction transaction =  new Gson().fromJson(remoteMessage.getData().get("transaction"), Transaction.class);
            addPayerExtra(intent, transaction);
        }


        intent.putExtra(Config.STEP, step);
        intent.setAction(SECURE_CHANNEL);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setContentTitle("MPCP");
        notificationBuilder.setContentText(remoteMessage.getNotification().getBody());
        notificationBuilder.setAutoCancel(true);
        notificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        notificationBuilder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void addPayerExtra(Intent intent, Transaction transaction) {
        Transaction savedTransaction = Transaction.retrieveById(this, transaction.getId());
        EncryptionProtocol protocol = ProtocolFactory.createProtocol(this, transaction.getProtocol());


        Log.i(LOG_TAG,"++++++++++++++++++++++++++Payer selected number: "+savedTransaction.getSelectedNumber());
        Log.i(LOG_TAG,"++++++++++++++++++++++++++Payee mid number: "+transaction.getPayeeMidNumber());
        Log.i(LOG_TAG,"++++++++++++++++++++++++++Payer shared key: "+
                protocol.calculateSharedKey(savedTransaction.getSelectedNumber(), transaction.getPayeeMidNumber()));


        long sharedStartTime = System.nanoTime();
        int calculatedSharedKey = protocol.calculateSharedKey(savedTransaction.getSelectedNumber(), transaction.getPayeeMidNumber());
        long sharedCalcTime = System.nanoTime() - sharedStartTime;
        long totalCalcTime = sharedCalcTime + Long.valueOf(transaction.getPayerMidCalcTime());
        intent.putExtra(Config.CALCULATED_SHARED_KEY, String.valueOf(calculatedSharedKey));
        intent.putExtra(Config.TOTAL_CALCULATED_TIME, String.valueOf(totalCalcTime));
        intent.putExtra(Config.PAYER_FLAG, String.valueOf(true));
        intent.putExtra(Config.TRANSACTION_ID, String.valueOf(transaction.getId()));
    }

    @NonNull
    private Intent getIntentForStep(int step) {
        Intent intent;
        switch (step)
        {
            case Transaction.STEP_ONE://This is received by the payee
                intent = new Intent(this, SecureChannelPayeeActivity.class);
                break;
            case Transaction.STEP_TWO://This is received by the payer
                intent = new Intent(this, SecureChannelEstablishActivity.class);
                break;
            default:
                intent = new Intent(this, HomeActivity.class);
                break;
        }
        return intent;
    }

}