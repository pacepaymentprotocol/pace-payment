package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PayeePaymentActivity extends AppCompatActivity {

    // This activity collects payee response for payment and acts based on this

    // Controls
    TextView fromLbl;
    TextView amountLbl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payee_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Controls
        // TODO Ahmed Labels texts should be fixed dynamically based on push from and amount
        fromLbl = (TextView) findViewById(R.id.moneyFromTxt);
        amountLbl = (TextView) findViewById(R.id.moneyAmountTxt);
        // TODO Act based on the clicked button accept or reject

        // Accept button
        Button acceptButton = (Button) findViewById(R.id.acceptBtn);
        acceptButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                // TODO Ahmed push the reply to both sides and insert transaction done into DB
            }
        });

        // Reject button
        Button rejectButton = (Button) findViewById(R.id.rejectBtn);
        rejectButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                // TODO Ahmed push the reply to sender (payer) and insert transaction fail into DB
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Set KB dismissal
        setTouchListenerForKeyboardDismissal();
    }

    public void setTouchListenerForKeyboardDismissal() {

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_payee_payment);
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motion) {

                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
