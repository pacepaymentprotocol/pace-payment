package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mohammad.mpcpapplication.models.Transaction;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;

import java.util.HashMap;

public class PayerPaymentActivity extends AppCompatActivity {

    // This activity initiates the payment activity: payer -> payee
    EditText amountEditText;
    TextView amountTextView;
    Button sendPaymentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payer_payment);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        sendPaymentButton = (Button)findViewById(R.id.sendPaymentButton);
        sendPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendPayment();
            }
        });
        amountTextView = (TextView)findViewById(R.id.amountTextView);
        amountEditText = (EditText)findViewById(R.id.amountTxt);
        amountEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                amountTextView.setText(String.format("$%s", charSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        setTouchListenerForKeyboardDismissal();
    }

    private void sendPayment() {
        Transaction transaction = Transaction.retrieveById(this, Integer.valueOf(getIntent().getStringExtra(Config.TRANSACTION_ID)));
        new SendPaymentTask(transaction).execute();
    }

    public void setTouchListenerForKeyboardDismissal() {

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_payer_payment);
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motion) {
                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }



    private class SendPaymentTask extends AsyncTask<Object, Object, String> {

        private Transaction transaction;
        private String amount;

        SendPaymentTask(Transaction transaction) {

            this.transaction = transaction;
            this.amount = amountEditText.getText().toString();
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put(Config.TRANSACTION_ID, String.valueOf(this.transaction.getId()));
            params.put(Config.TRANSACTION_AMOUNT,  this.amount);
            return FunctionHelper.apiCaller("transaction/send-payment", params);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                Intent intent = new Intent(PayerPaymentActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        }
    }


}
