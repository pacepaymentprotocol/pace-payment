package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mohammad.mpcpapplication.models.User;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;
import com.example.mohammad.mpcpapplication.utils.Preferences;
import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {
    private static final String LOG_TAG = RegisterActivity.class.getSimpleName();

    // This is the register activity

    // Controls
    EditText mobileText;
    EditText usernameText;
    EditText passwordText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initialize preferences
        Preferences.init(this);
        int timestamp1 = Preferences.getInteger(Config.TIMESTAMP1);
        Log.d(LOG_TAG,"Timestamp1: "+timestamp1);

        // Controls
        mobileText = (EditText)findViewById(R.id.mobileTxt);
        usernameText = (EditText)findViewById(R.id.emailTxt);
        passwordText = (EditText)findViewById(R.id.passwordTxt);

        // Register button
        Button registerButton = (Button) findViewById(R.id.registerBtn);
        registerButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                if(mobileText.getText().toString().trim().equals("")
                        || usernameText.getText().toString().trim().equals("")
                        || passwordText.getText().toString().trim().equals("")){
                    Toast.makeText(RegisterActivity.this, "Fill out all the fields", Toast.LENGTH_LONG).show();
                }else{
                    new RegisterTask(
                            usernameText.getText().toString(),
                            passwordText.getText().toString(),
                            mobileText.getText().toString()
                    ).execute();

                }
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Set keyboard dismissal
        setTouchListenerForKeyboardDismissal();
    }

    public void setTouchListenerForKeyboardDismissal() {

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_register);
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motion) {

                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }


    private class RegisterTask extends AsyncTask<Object, Object, String> {

        private final String username;
        private final String password;
        private final String mobile;


        RegisterTask(String username, String password, String mobile){
            this.username = username;
            this.password = password;
            this.mobile = mobile;
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put("email", this.username);
            params.put("password", this.password);
            params.put("mobile", this.mobile);
            return FunctionHelper.apiCaller("auth/register", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "Executed ... " + result);
            if(result != null){
                Toast.makeText(RegisterActivity.this, "Registration success!", Toast.LENGTH_LONG).show();
                Intent loginPage = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(loginPage);
            }else{
                Toast.makeText(RegisterActivity.this, "Failed to register", Toast.LENGTH_LONG).show();
            }
        }
    }

}
