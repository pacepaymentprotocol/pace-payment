package com.example.mohammad.mpcpapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Preferences;

public class SecureChannelEstablishActivity extends AppCompatActivity {

    private static final String LOG_TAG = SecureChannelEstablishActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(LOG_TAG,"Secure channel establish activity");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure_channel_establish);
        Preferences.init(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String calcSharedKey = getIntent().getStringExtra(Config.CALCULATED_SHARED_KEY);
        String totalCalcTime = getIntent().getStringExtra(Config.TOTAL_CALCULATED_TIME);

        TextView sharedKeyTextView = (TextView) findViewById(R.id.sharedKeyTextView);
        sharedKeyTextView.setText(String.format("Your shared key value is %s", calcSharedKey));

        TextView calculatedSharedKeyTextView = (TextView) findViewById(R.id.totalTimeTextView);
        calculatedSharedKeyTextView.setText(String.format("Total calculated time in %s nano seconds", totalCalcTime));


        Button nextButton = (Button) findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextAction();
            }

        });
    }

    private void nextAction() {
        String payerFlag = getIntent().getStringExtra(Config.PAYER_FLAG);

        if(payerFlag == null){
            startActivity(new Intent(this, HomeActivity.class));
        }else{
            Intent intent = new Intent(this, PayerPaymentActivity.class);
            intent.putExtra(Config.TRANSACTION_ID, getIntent().getStringExtra(Config.TRANSACTION_ID));
            startActivity(intent);
        }
    }
}
