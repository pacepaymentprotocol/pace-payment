package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mohammad.mpcpapplication.models.Transaction;
import com.example.mohammad.mpcpapplication.protocol.EncryptionProtocol;
import com.example.mohammad.mpcpapplication.protocol.ProtocolFactory;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;
import com.example.mohammad.mpcpapplication.utils.Preferences;
import com.google.gson.Gson;

import java.util.HashMap;


public class SecureChannelPayeeActivity extends AppCompatActivity {

    private static final String LOG_TAG = SecureChannelPayeeActivity.class.getSimpleName();
    private EncryptionProtocol protocol;
    private Context context = this;
    private Transaction transaction;
    private TextView methodNameTextView;
    private TextView mobileTextView;
    private NumberPicker numberPicker;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.i(LOG_TAG,"Secure channel payee activity");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure_channel_payee);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        methodNameTextView = (TextView) findViewById(R.id.methodNameTxt);
        mobileTextView = (TextView) findViewById(R.id.mobileTextView);

        // Initialize shared preferences
        Preferences.init(this);


        // Controls
        // Request Secure Channel button
        Button establishSecureChannelButton = (Button) findViewById(R.id.establishSecureChannelBtn);
        establishSecureChannelButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                establishSecureChannel();
            }
        });

        // Set KB dismissal
        setTouchListenerForKeyboardDismissal();

        initializeNumberPicker();


        int id = getIntent().getExtras().getInt(Config.TRANSACTION_ID, 0);
        if(id != 0){
            new GetTransactionDataTask().execute("transaction/retrieve/" + id);
        }
    }





    private class GetTransactionDataTask extends AsyncTask<String, Object, String> {

        protected String doInBackground(String... urls) {
            return FunctionHelper.apiCaller(urls[0], new HashMap<String, String>());
        }

        protected void onPostExecute(String result) {
            if(result != null){
                transaction = new Gson().fromJson(result, Transaction.class);
                methodNameTextView.setText(transaction.getProtocol());
                mobileTextView.setText("Secure Channel Request From:" + transaction.getPayer().getMobile());
            }
        }
    }




    private void initializeNumberPicker() {
        final TextView textView = (TextView) findViewById(R.id.tv);
        numberPicker = (NumberPicker) findViewById(R.id.np);
        textView.setTextColor(Color.parseColor("#ffd32b3b"));
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(9);
        numberPicker.setValue(1);
        numberPicker.setWrapSelectorWheel(true);
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                textView.setText(String.format("Selected Number : " + newVal));

            }
        });

    }

    public void setTouchListenerForKeyboardDismissal() {

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_secure_channel_payee);
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motion) {

                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {

        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void establishSecureChannel() {
        new EstablishSecureChannelTask(transaction).execute();
    }


    private class EstablishSecureChannelTask extends AsyncTask<Object, Object, String> {
        private final int calculatedSharedKey;
        private final long totalCalcTime;
        private Transaction transaction;
        private int payeeMiddleNumber;
        private long middleCalcTime;
        private final long sharedCalcTime;

        EstablishSecureChannelTask(Transaction transaction) {
            this.transaction = transaction;
            protocol = ProtocolFactory
                    .createProtocol(context, transaction.getProtocol());


            Log.i(LOG_TAG,"++++++++++++++++++++++++");
            Log.i(LOG_TAG,"++++++++++++++++++++++++Protocol: "+transaction.getProtocol());
            Log.i(LOG_TAG,"++++++++++++++++++++++++Payee selected number: "+numberPicker.getValue());
            Log.i(LOG_TAG,"++++++++++++++++++++++++Payee middle number: "+protocol.calculateMiddleKey(numberPicker.getValue()));
            Log.i(LOG_TAG,"++++++++++++++++++++++++Payee shared key: "+protocol.calculateSharedKey(numberPicker.getValue(), transaction.getPayerMidNumber()));

            long middleStartTime = System.nanoTime();
            this.payeeMiddleNumber = protocol.calculateMiddleKey(numberPicker.getValue());
            this.middleCalcTime = System.nanoTime() - middleStartTime;
            long sharedStartTime = System.nanoTime();
            this.calculatedSharedKey = protocol.calculateSharedKey(numberPicker.getValue(), transaction.getPayerMidNumber());
            this.sharedCalcTime = System.nanoTime() - sharedStartTime;
            this.totalCalcTime = this.sharedCalcTime + this.middleCalcTime;
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put(Config.TRANSACTION_ID, String.valueOf(this.transaction.getId()));

            Log.i(LOG_TAG,"Payee mid number: "+String.valueOf(this.payeeMiddleNumber));



            params.put("payee_mid_number", String.valueOf(this.payeeMiddleNumber));
            params.put("payee_mid_calc_time", String.valueOf(this.middleCalcTime));
            params.put("payee_shared_calc_time", String.valueOf(this.sharedCalcTime));
            return FunctionHelper.apiCaller("transaction/establish", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "EstablishSecureChannelTask = " + result);
            if (result != null) {
                Transaction transaction = new Gson().fromJson(result, Transaction.class);
                transaction.setSelectedNumber(numberPicker.getValue());
                transaction.setCalculatedSharedKey(this.calculatedSharedKey);
                transaction.save(context);
                Intent intent = new Intent(SecureChannelPayeeActivity.this, SecureChannelEstablishActivity.class);
                intent.putExtra(Config.CALCULATED_SHARED_KEY, String.valueOf(this.calculatedSharedKey));
                intent.putExtra(Config.TOTAL_CALCULATED_TIME, String.valueOf(this.totalCalcTime));
                startActivity(intent);
            }
        }
    }

}
