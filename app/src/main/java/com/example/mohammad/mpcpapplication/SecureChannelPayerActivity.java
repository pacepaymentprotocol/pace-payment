package com.example.mohammad.mpcpapplication;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mohammad.mpcpapplication.models.Transaction;
import com.example.mohammad.mpcpapplication.models.User;
import com.example.mohammad.mpcpapplication.protocol.EncryptionProtocol;
import com.example.mohammad.mpcpapplication.protocol.ProtocolFactory;
import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.FunctionHelper;
import com.example.mohammad.mpcpapplication.utils.Preferences;
import com.google.gson.Gson;

import java.util.HashMap;

public class SecureChannelPayerActivity extends AppCompatActivity {


    private static final String LOG_TAG = SecureChannelPayerActivity.class.getSimpleName();

    private EncryptionProtocol protocol;
    private User payee;
    private int selectedNumber = 1;
    public Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure_channel_payer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        final EditText payeeMobile = (EditText) findViewById(R.id.payeeMobileTxt);
        Preferences.init(this);

        String method = Preferences.getString(Config.DEFAULT_METHOD);
        Log.d(LOG_TAG, "method  = " + method);

        TextView methodTextView = (TextView) findViewById(R.id.textView2);
        methodTextView.setText(method);

        // Request Secure Channel button
        Button requestSecureChannelButton = (Button) findViewById(R.id.requestSecureChannelBtn);
        requestSecureChannelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new GetUserByMobileTask(payeeMobile.getText().toString()).execute();
            }
        });


        setTouchListenerForKeyboardDismissal();

        initializeNumberPicker();
    }

    private void initializeNumberPicker() {
        final TextView textView = (TextView) findViewById(R.id.tv);
        NumberPicker numberPicker = (NumberPicker) findViewById(R.id.np);
        textView.setTextColor(Color.parseColor("#ffd32b3b"));
        numberPicker.setMinValue(1);
        numberPicker.setMaxValue(9);
        numberPicker.setValue(selectedNumber);
        numberPicker.setWrapSelectorWheel(true);

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                textView.setText(String.format("Selected Number: " + newVal));
                selectedNumber = newVal;
            }
        });

    }

    private class RequestSecureChanelTask extends AsyncTask<Object, Object, String> {
        private final User payee;
        private int middleNumber;
        private long middleCalcTime;

        RequestSecureChanelTask(User payee) {
            this.payee = payee;
            long startTime = System.nanoTime();
            this.middleNumber = ProtocolFactory
                    .createProtocol(context, Preferences.getString(Config.DEFAULT_METHOD))
                    .calculateMiddleKey(selectedNumber);
            this.middleCalcTime = System.nanoTime() - startTime;

            Log.d(LOG_TAG, "selectedNumber = " + selectedNumber);
            Log.d(LOG_TAG, "middleNumber = " + this.middleNumber);
            Log.d(LOG_TAG, "middleCalcTime = " + this.middleCalcTime);
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put("payee_id", payee.getId().toString());
            params.put("payer_mid_number", String.valueOf(this.middleNumber));
            params.put("payer_id", String.valueOf(Preferences.getInteger(Config.USER_ID)));
            params.put("protocol", Preferences.getString(Config.DEFAULT_METHOD));
            params.put("payer_mid_calc_time", String.valueOf(this.middleCalcTime));
            return FunctionHelper.apiCaller("transaction/initiate", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "transaction created = " + result);
            if (result != null) {
                Transaction transaction = new Gson().fromJson(result, Transaction.class);
                transaction.setSelectedNumber(selectedNumber);
                transaction.save(context);
                new NotifyUserTask(payee, transaction).execute();
            }

        }
    }


    private class NotifyUserTask extends AsyncTask<Object, Object, String> {
        private final User payee;
        private Integer transactionId;

        NotifyUserTask(User payee, Transaction transaction) {
            this.payee = payee;
            this.transactionId = transaction.getId();
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put("payee_id", payee.getId().toString());
            params.put(Config.TRANSACTION_ID, this.transactionId.toString());
            return FunctionHelper.apiCaller("transaction/notify", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "user notified = " + result);
            if (result != null) {
                Toast.makeText(context, "Secure channel request has been sent.", Toast.LENGTH_SHORT).show();
                Intent securePayeeIntent = new Intent(SecureChannelPayerActivity.this, HomeActivity.class);
                startActivity(securePayeeIntent);
            }
        }
    }

    private class GetUserByMobileTask extends AsyncTask<Object, Object, String> {
        String mobile;

        GetUserByMobileTask(String mobile) {
            this.mobile = mobile;
        }

        protected String doInBackground(Object... urls) {
            HashMap<String, String> params = new HashMap<>();
            params.put("mobile", this.mobile);
            return FunctionHelper.apiCaller("user/find-by-mobile", params);
        }

        protected void onPostExecute(String result) {
            Log.d(LOG_TAG, "Executed ... " + result);
            if (result != null) {
                payee = new Gson().fromJson(result, User.class);
                new RequestSecureChanelTask(payee).execute();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(SecureChannelPayerActivity.this);
                builder.setMessage("Failed to get the user info")
                        .setTitle("error");
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        }
    }


    public void setTouchListenerForKeyboardDismissal() {
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.content_secure_channel_payer);
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View view, MotionEvent motion) {
                hideKeyboard();
                return false;
            }
        });
    }

    protected void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
