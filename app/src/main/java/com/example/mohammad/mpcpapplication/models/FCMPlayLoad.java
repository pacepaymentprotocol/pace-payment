package com.example.mohammad.mpcpapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class FCMPlayLoad {

    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("notification")
    @Expose
    private Notification notification;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    private class Notification {

        @SerializedName("body")
        @Expose
        private String body;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("icon")
        @Expose
        private String icon;

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

    }



    private class Data {

        @SerializedName("transaction_id")
        @Expose
        private String transactionId;

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

    }

}
