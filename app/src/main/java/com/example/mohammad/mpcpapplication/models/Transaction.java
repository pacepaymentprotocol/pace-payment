package com.example.mohammad.mpcpapplication.models;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Preferences;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;



public class Transaction {

    private static final String LOG_TAG = Transaction.class.getSimpleName();

    public static final int STEP_ONE = 1;
    public static final int STEP_TWO = 2;

    @SerializedName("payer_mid_number")
    @Expose
    private int payerMidNumber;
    @SerializedName("payee_mid_number")
    @Expose
    private int payeeMidNumber;
    @SerializedName("payer_id")
    @Expose
    private String payerId;
    @SerializedName("payee_id")
    @Expose
    private String payeeId;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("protocol")
    @Expose
    private String protocol;

    @SerializedName("payer")
    @Expose
    private User payer;

    @SerializedName("payee")
    @Expose
    private User payee;


    @SerializedName("payer_mid_calc_time")
    @Expose
    private String payerMidCalcTime;

    private int selectedNumber;
    private int sharedKey;
    private int calculatedSharedKey;


    public int getPayerMidNumber() {
        return payerMidNumber;
    }

    public void setPayerMidNumber(int payerMidNumber) {
        this.payerMidNumber = payerMidNumber;
    }

    public String getPayerId() {
        return payerId;
    }

    public void setPayerId(String payerId) {
        this.payerId = payerId;
    }


    public String getPayerMidCalcTime() {
        return payerMidCalcTime;
    }

    public void setPayerMidCalcTime(String payerMidCalcTime) {
        this.payerMidCalcTime = payerMidCalcTime;
    }


    public String getPayeeId() {
        return payeeId;
    }

    public void setPayeeId(String payeeId) {
        this.payeeId = payeeId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public int getSelectedNumber() {
        return selectedNumber;
    }

    public void setSelectedNumber(int selectedNumber) {
        this.selectedNumber = selectedNumber;
    }

    public int getSharedKey() {
        return sharedKey;
    }

    public void setSharedKey(int sharedKey) {
        this.sharedKey = sharedKey;
    }


    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }


    public User getPayer() {
        return payer;
    }

    public void setPayer(User payer) {
        this.payer = payer;
    }

    public User getPayee() {
        return payee;
    }

    public void setPayee(User payee) {
        this.payee = payee;
    }

    public void save(Context context) {
        HashMap<Integer, Transaction> hashMap = new HashMap<>();
        hashMap.put(this.id, this);

        Gson gson = new Gson();
        String hashMapString = gson.toJson(hashMap);

        Preferences.init(context);
        Preferences.putString(Config.TRANSACTIONS, hashMapString);
    }

    public static Transaction retrieveById(Context context, int id) {
        Preferences.init(context);
        String hashMapString = Preferences.getString(Config.TRANSACTIONS);
        Log.d(LOG_TAG, hashMapString);

        java.lang.reflect.Type type = new TypeToken<HashMap<Integer, Transaction>>(){}.getType();
        HashMap<Integer, Transaction> hashMap = new Gson().fromJson(hashMapString, type);
        Transaction transaction = hashMap.get(id);
        return transaction;
    }

    public void setCalculatedSharedKey(int calculatedSharedKey) {
        this.calculatedSharedKey = calculatedSharedKey;
    }

    public int getCalculatedSharedKey() {
        return calculatedSharedKey;
    }

    public int getPayeeMidNumber() {
        return payeeMidNumber;
    }

    public void setPayeeMidNumber(int payeeMidNumber) {
        this.payeeMidNumber = payeeMidNumber;
    }
}