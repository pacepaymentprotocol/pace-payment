package com.example.mohammad.mpcpapplication.protocol;

import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Preferences;

public class DiffieHellman implements EncryptionProtocol {

    private int generator = 3;
    private int primeNumber = 13;

    @Override
    public int calculateMiddleKey(int selectedNumber) {
        int payerMiddleNumber =  (int) (Math.pow(generator, selectedNumber) % primeNumber);
        return payerMiddleNumber;
    }

    @Override
    public int calculateSharedKey(int selectedNumber, int otherPersonMiddleNumber) {
        int sharedKey = (int) (Math.pow(otherPersonMiddleNumber,selectedNumber) % primeNumber);
        return sharedKey;
    }

}
