package com.example.mohammad.mpcpapplication.protocol;

public interface EncryptionProtocol {
    int calculateMiddleKey(int selectedNumber);
    int calculateSharedKey(int selectedNumber, int otherPersonMiddleNumber);

}
