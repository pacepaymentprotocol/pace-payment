package com.example.mohammad.mpcpapplication.protocol;

public class PaceProtocol implements EncryptionProtocol {

    private int generator = 3;
    int primeNumber = 5;

    @Override
    public int calculateMiddleKey(int selectedNumber) {
        double c = Math.log(generator);
        int payerMiddleNumber = (int) (((selectedNumber % primeNumber) * (Math.log(generator) / c)) % primeNumber) % primeNumber;
        return payerMiddleNumber;
    }

    @Override
    public int calculateSharedKey(int selectedNumber, int otherPersonMiddleNumber) {
        int sharedKey = (((selectedNumber%primeNumber)*(otherPersonMiddleNumber))%primeNumber) % primeNumber;
        return sharedKey;
    }

}
