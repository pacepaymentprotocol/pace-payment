package com.example.mohammad.mpcpapplication.protocol;

import android.content.Context;

import com.example.mohammad.mpcpapplication.utils.Config;
import com.example.mohammad.mpcpapplication.utils.Preferences;

public class ProtocolFactory {

    public static EncryptionProtocol createProtocol(Context context, String protocol)
    {
        Preferences.init(context);
        if (Preferences.getString(Config.DEFAULT_METHOD).equals(Config.PACE_PROTOCOL)) {
            return new PaceProtocol();
        } else{
            return new DiffieHellman();
        }
    }
}
