package com.example.mohammad.mpcpapplication.utils;

public class Config {
    public static final String PAYER_PROTOCOL_NUMBER = "payer_protocol_number";
    public static final String PAYEE_PROTOCOL_NUMBER = "payee_protocol_number";
    public static final String USER_EMAIL = "user_email";
    public static final String LOGGED_IN = "logged_in";
    public static final String STEP = "step";
    public static final String TRANSACTION_AMOUNT = "amount";
    static String API_URL  = "http://gloriousoft.com/api/";
    public static String DEFAULT_METHOD = "default_method";
    public final static String DIFFIE_HELLMAN = "diffie-hellman";
    public final static String PACE_PROTOCOL = "pace_protocol";
    public final static String TIMESTAMP1 = "timestamp1";
    public final static String TIMESTAMP2 = "timestamp2";
    public final static String TIMESTAMP3 = "timestamp3";
    public final static String a = "a";
    public final static String b = "b";
    public final static String A = "A";
    public final static String B = "B";
    public final static String u = "u";
    public final static String v = "v";
    public final static String X = "X";
    public final static String Y = "Y";
    public final static String SHARED_KEY = "key_key";

    public final static String USER_ID = "user_id";

    public static final String REG_TOKEN = "REG_TOKEN";
    public static String USER_PASSWORD = "user_password";
    public static String TRANSACTIONS = "transactions";
    public static String TRANSACTION_ID = "transaction_id";
    public static String CALCULATED_SHARED_KEY = "CALCULATED_SHARED_KEY";
    public static String TOTAL_CALCULATED_TIME = "TOTAL_CALCULATED_TIME";

    public static String PAYER_FLAG = "PAYER_FLAG";
}
