package com.example.mohammad.mpcpapplication;

import com.example.mohammad.mpcpapplication.protocol.DiffieHellman;
import com.example.mohammad.mpcpapplication.protocol.PaceProtocol;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for our encryption protocols
 */
public class UnitTest {

    @Test
    public void pace_protocol_test() {
        // Payer selected number: 8, Payee selected number: 9, Payer mid number: 3, Payee mid number: 4
        // Payer key: 2, Payee key: 2
        PaceProtocol paceProtocol = new PaceProtocol();
        int payerSelectedNumber = 8;
        int payeeSelectedNumber = 9;
        // Calculate payer middle key
        assertEquals(3, paceProtocol.calculateMiddleKey(payerSelectedNumber));
        // Calculate payee middle key
        assertEquals(4, paceProtocol.calculateMiddleKey(payeeSelectedNumber));
        // Calculate payer shared key
        assertEquals(2, paceProtocol.calculateSharedKey(payerSelectedNumber,paceProtocol.calculateMiddleKey(payeeSelectedNumber)));
        // Calculate payee shared key
        assertEquals(2, paceProtocol.calculateSharedKey(payeeSelectedNumber,paceProtocol.calculateMiddleKey(payerSelectedNumber)));
    }

    @Test
    public void dh_protocol_test() {
        // Payer selected number: 5, Payee selected number: 7, Payer mid number: 9, Payee mid number: 3
        // Payer key: 9, Payee key: 9
        DiffieHellman diffieHellman = new DiffieHellman();
        int payerSelectedNumber = 5;
        int payeeSelectedNumber = 7;
        // Calculate payer middle key
        assertEquals(9, diffieHellman.calculateMiddleKey(payerSelectedNumber));
        // Calculate payee middle key
        assertEquals(3, diffieHellman.calculateMiddleKey(payeeSelectedNumber));
        // Calculate payer shared key
        assertEquals(9, diffieHellman.calculateSharedKey(payerSelectedNumber,diffieHellman.calculateMiddleKey(payeeSelectedNumber)));
        // Calculate payee shared key
        assertEquals(9, diffieHellman.calculateSharedKey(payeeSelectedNumber,diffieHellman.calculateMiddleKey(payerSelectedNumber)));
    }
}